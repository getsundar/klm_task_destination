'use strict';

/**
 * @ngdoc overview
 * @name klmApp
 * @description
 * # klmApp
 *
 * Main module of the application.
 */
angular
    .module('klmApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap'
    ])
    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('destinations', {
                name: 'destinations',
                url: '/destinations',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            });
        $urlRouterProvider.otherwise('/destinations');
    });
