'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('MainCtrl', ['$scope', '$http', '$filter', 'destinationService', function($scope, $http, $filter, destinationService) {

        $scope.textLimit = 300;
        $scope.showHeader = false;
        $scope.currentLayout = "list";
        $scope.floatingContainerDynamicClass = "floatingModule__container";

        destinationService.getCityDetails($scope);

        // Setting filter types      
        $scope.sortingTypes = [
            { displayName: "Most Popular", value: "mp" },
            { displayName: "Price(low-high)", value: "plh" },
            { displayName: "Price(high-low)", value: "phl" },
            { displayName: "Alphabetic(a-z)", value: "aaz" },
            { displayName: "Alphabetic(z-a)", value: "aza" },
            { displayName: "Flight Duration(short-long)", value: "fdsl" },
            { displayName: "Flight Duration(long-short)", value: "fdls" },
        ];

        $scope.$watch('sortingType', function(newValue) {
            if (newValue) {
                switch (newValue.value) {
                    case 'mp':
                        $scope.items = $filter('orderBy')($scope.items, 'bookedCount');
                        break;
                    case 'aaz':
                        $scope.items = $filter('orderBy')($scope.items, 'country');
                        break;
                    case 'aza':
                        $scope.items = $filter('orderBy')($scope.items, '-country');
                        break;
                    case 'plh':
                        $scope.items = $filter('orderBy')($scope.items, 'returnFare');
                        break;
                    case 'phl':
                        $scope.items = $filter('orderBy')($scope.items, '-returnFare');
                        break;
                    case 'fdsl':
                        $scope.items = $filter('orderBy')($scope.items, 'travelMins');
                        break;
                    case 'fdls':
                        $scope.items = $filter('orderBy')($scope.items, '-travelMins');
                        break;

                    default:

                }
            }
        });
        $scope.showContent = function(module) {
            $scope.floatingModule = module;
            $scope.floatingContainerDynamicClass = "floatingModule__container__Running";
            if ($scope.floatingModule == "inspired") {
                $scope.floatingTitle = "Try our hand-picked travel ideas";
            } else {
                $scope.floatingTitle = "Narrow down your options";
            }

        }
        $scope.closeFloatingDiv = function() {
            $scope.floatingContainerDynamicClass = "floatingModule__container__Reverse";
        }
        $scope.showHideHeader = function() {
            $scope.showHeader = !$scope.showHeader;
        }
        $scope.changeLayout = function(layout) {

            $scope.currentLayout = layout;

        };
        $scope.onNavigationKeyPress = function($event, navSelected) {
            if ($event.keyCode === 13) {
                if (navSelected == "showHeader") {
                    this.showHideHeader();

                } else {
                    this.showContent(navSelected)
                }
            }
        }

    }])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);
