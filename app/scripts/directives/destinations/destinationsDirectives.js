angular.module('klmApp')

.directive('routeDetails', function() {
    return {
        restrict: 'E',
        scope: {
            item: '='
        },
        controller: ['$scope', function($scope) {
            $scope.Math = window.Math;
        }],
        templateUrl: "views/templates/destinations/citySearchTemplates.html"
    };
})

.directive('nagvigationContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/navigationTemplates.html"
    };
})



.directive('headerContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/headerTemplate.html"
    };
})


.directive('modalPopup', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/modalPopupTemplate.html"
    };
})

.directive('floatingContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/floatingContentTemplates.html"
    };
})

.directive('inspiredContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/inspiredTemplates.html"
    };
})

.directive('filterfindContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/filterfindTemplates.html"
    };
})

.directive('gridsearchContent', function() {
    return {
        restrict: 'E',
        templateUrl: "views/templates/destinations/gridsearchTemplates.html"
    };
})
