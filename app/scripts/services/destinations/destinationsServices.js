angular.module('klmApp')
    .factory('httpInterceptor', ['$rootScope', function($rootScope) {
        return {
            request: function(config) {
                return config;
            },
            response: function(response) {
                $rootScope.loaded = true;
                return response;
            },

            requestError: function(rejection) {
                console.log('ERR:' + JSON.stringify(rejection));
            },
            responseError: function(rejection) {
                console.log('ERR:' + JSON.stringify(rejection));
            }
        };
    }])
    .service('destinationService', ['$http', function($http) {

        this.getCityDetails = function($scope) {

            $http.get("data/searchdetails.json")
                .then(function(response) {                    
                    $scope.items = response.data;
                });
        }


    }])
